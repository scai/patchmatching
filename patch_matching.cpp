#include "patch_matching.h"
#include <fstream>

using namespace std;
using namespace cv;
using namespace cv::cuda;

PatchMatching::PatchMatching()
{
}

PatchMatching::~PatchMatching()
{}

void PatchMatching::set_src(const Mat& src){
	_src_img = src;
#ifdef COMPUTE_GPU
	_src_img_gpu.upload(_src_img);
#endif
}

void PatchMatching::set_tar(const Mat& tar){
	_tar_img = tar;
#ifdef COMPUTE_GPU
	_tar_img_gpu.upload(_tar_img);
#endif
}

void PatchMatching::set_src_patch(int x , int y, int width, int height){
	Rect temp(x, y, width, height);
	_src_patch = _src_img(temp);
#ifdef COMPUTE_GPU
		_src_patch_gpu.upload(_src_patch);
#endif
}
void PatchMatching::set_tar_patch(int x, int y , int width, int height){
	Rect temp(x, y, width, height);
	_tar_patch = _tar_img(temp);
#ifdef COMPUTE_GPU
	_tar_patch_gpu.upload(_tar_patch);
#endif
}

double PatchMatching::get_confidence_gpu(){
	double confidence = 0;
	int numRows1 = _src_patch.rows;
	int numCols1 = _src_patch.cols;
	int numRows2 = _tar_patch.rows;
	int numCols2 = _tar_patch.cols;
	
	confidence = match_gpu();
	return confidence;
}

Rect PatchMatching::get_best_buddies_gpu(){
	Rect res;
	int numRows1 = _src_img.rows;
	int numCols1 = _src_img.cols;
	int N1 = numRows1 * numCols1;
	int lambda = 2;

	int sampleStep = 5;
	double maxConfidence = 0;


	ofstream out("record_gpu.txt");
	int64 t0 = cv::getTickCount();
	for (int x = 0; x <= _tar_img.cols - _src_patch.cols; x += sampleStep){
		for (int y = 0; y <= _tar_img.rows - _src_patch.rows; y += sampleStep){
			out << x << ", " << y << "\t";
			int numRows2 = numRows1;
			int numCols2 = numCols1;
			int N2 = numRows2 * numCols2;
			set_tar_patch( x, y , _src_patch.cols, _src_patch.rows);

			double confidence = match_gpu();
			out << confidence << endl;
			if (confidence > maxConfidence){
				maxConfidence = confidence;
				res = Rect(x, y, _src_patch.cols, _src_patch.rows);
			}
		}
	}
	out.close();
	cout << "takes: " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 << " ms. " << endl;
	return res;
}


double PatchMatching::get_confidence(){
	Rect res;
	Mat splitted_s[3];
	split(_src_patch, splitted_s);
	int numRows = _src_patch.rows;
	int numCols = _src_patch.cols;
	int N1 = numRows * numCols;
	int numRowt = _tar_patch.rows;
	int numColt = _tar_patch.cols;
	int N2 = numRowt* numColt;
	int lambda = 2;

	Mat distance = Mat::zeros(N1, N2, CV_32FC1);
	Mat splitted_t[3];
	split(_tar_patch, splitted_t);
	for (int i = 0; i < N1; i++){
		for (int j = 0; j < N2; j++){
			//cout << i << ", " << j << endl;
			int diffR = (int)splitted_s[0].at<uchar>(i / numCols, i % numCols) - splitted_t[0].at<uchar>(j / numColt, j % numColt);
			int diffG = (int)splitted_s[1].at<uchar>(i / numCols, i % numCols) - splitted_t[1].at<uchar>(j / numColt, j % numColt);
			int diffB = (int)splitted_s[2].at<uchar>(i / numCols, i % numCols) - splitted_t[2].at<uchar>(j / numColt, j % numColt);
			int distanceXY = (i / numCols - j / numColt) * (i / numCols - j / numColt) + (i % numCols - j % numColt) * (i % numCols - j % numColt);
			distance.at<float>(i, j) = lambda * sqrt(diffR * diffR + diffG * diffG + diffB * diffB) / (255 * sqrt(3));// +(float)sqrt(distanceXY) / sqrt(numRows*numRows + numCols * numCols);
		}
	}

	vector<int> array1(N1, 0);
	for (int i = 0; i < N1; i++){
		float dist = 0.1;
		int index = -1;
		for (int j = 0; j < N2; j++){
			if (distance.at<float>(i, j) < dist){
				dist = distance.at<float>(i, j);
				index = j;
			}
		}
		array1[i] = index;
	}

	vector<int> array2(N2, 0);
	for (int i = 0; i < N2; i++){
		float dist = 0.1;
		int index = -1;
		for (int j = 0; j < N1; j++){
			if (distance.at<float>(j, i) < dist){
				dist = distance.at<float>(j, i);
				index = j;
			}
		}
		array2[i] = index;
	}

	int count = 0;
	for (int i = 0; i < N2; i++){
		if (array2[i] != -1 && array1[array2[i]] == i)
			count++;
	}
	
	int smaller = N1 < N2 ? N1 : N2;
	double confidence = (double)count / (double)smaller;
	return confidence;
}
Rect PatchMatching::get_best_buddies(){
	Rect res;
	
	Mat splitted_s[3];
	split(_src_patch, splitted_s);
	int numRows = _src_patch.rows;
	int numCols = _src_patch.cols;
	int N1 = numRows * numCols;
	int lambda = 2;

	int sampleStep = 5;
	double maxConfidence = 0.0;


	//ofstream out("result.txt");
	//#pragma omp parallel for
	for (int x = 0; x <= _tar_img.cols - _src_patch.cols; x += sampleStep){
		for (int y = 0; y <= _tar_img.rows - _src_patch.rows; y += sampleStep){
			for (int w = numCols*0.8; w <= numCols*1.2; w+=sampleStep){
				for (int h = numRows * 0.8; h <= numRows*1.2; h+=sampleStep){
					set_tar_patch(x, y, min(w,_tar_img.cols - x), min(h,_tar_img.rows - y));
					int numRowt = _tar_patch.rows;
					int numColt = _tar_patch.cols;
					int N2 = numRowt* numColt;

					Mat distance = Mat::zeros(N1, N2, CV_32FC1);
					Mat splitted_t[3];
					split(_tar_patch, splitted_t);

					for (int i = 0; i < N1; i++){
						for (int j = 0; j < N2; j++){
							//cout << i << ", " << j << endl;
							int diffR = (int)splitted_s[0].at<uchar>(i / numCols, i % numCols) - splitted_t[0].at<uchar>(j / numColt, j % numColt);
							int diffG = (int)splitted_s[1].at<uchar>(i / numCols, i % numCols) - splitted_t[1].at<uchar>(j / numColt, j % numColt);
							int diffB = (int)splitted_s[2].at<uchar>(i / numCols, i % numCols) - splitted_t[2].at<uchar>(j / numColt, j % numColt);
							int distanceXY = (i / numCols - j / numColt) * (i / numCols - j / numColt) + (i % numCols - j % numColt) * (i % numCols - j % numColt);
							distance.at<float>(i, j) = lambda * sqrt(diffR * diffR + diffG * diffG + diffB * diffB) / (255 * sqrt(3));// +(float)sqrt(distanceXY) / sqrt(numRows*numRows + numCols * numCols);
						}
					}

					vector<int> array1(N1, 0);
					for (int i = 0; i < N1; i++){
						float dist = 0.1;
						int index = -1;
						for (int j = 0; j < N2; j++){
							if (distance.at<float>(i, j) < dist){
								dist = distance.at<float>(i, j);
								index = j;
							}
						}
						array1[i] = index;
					}

					vector<int> array2(N2, 0);
					for (int i = 0; i < N2; i++){
						float dist = 0.1;
						int index = -1;
						for (int j = 0; j < N1; j++){
							if (distance.at<float>(j, i) < dist){
								dist = distance.at<float>(j, i);
								index = j;
							}
						}
						array2[i] = index;
					}

					int count = 0;
					for (int i = 0; i < N2; i++){
						if (array2[i] != -1 && array1[array2[i]] == i)
							count++;
					}
					cout << "count: " << count << endl;
					int smaller = N1 < N2 ? N1 : N2;
					double confidence = (double)count / (double)smaller;
					//#pragma omp critical
					if (confidence > maxConfidence){
						maxConfidence = confidence;
						res = Rect(x,y,w,h);
					}
				}
			}
		}
	}
	//out.close();
	return res;
}
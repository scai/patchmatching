﻿#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <cuda_device_runtime_api.h>
#include <device_launch_parameters.h>
#include "opencv2/opencv.hpp"
#include "opencv2/cudaarithm.hpp"
#include "utils.h"
#include "patch_matching.h"



using namespace cv;
using namespace std;
using namespace cv::cuda;

__global__ void matching(const PtrStepSz<uchar> d_srcb, const PtrStepSz<uchar> d_srcg, const PtrStepSz<uchar> d_srcr, const PtrStepSz<uchar> d_tarb, const PtrStepSz<uchar> d_targ, const PtrStepSz<uchar> d_tarr, PtrStepSz<double> d_distance,
	int numRows1, int numCols1, int numRows2, int numCols2){
	const int c = blockIdx.x* blockDim.x + threadIdx.x;
	const int r = blockIdx.y * blockDim.y + threadIdx.y;

	if (c >= numCols1 || c < 0 || r >= numRows1 || r < 0) return;


	int lambda = 2;

	for (int i = 0; i < numRows2; i++){
		for (int j = 0; j < numCols2; j++){
			
				int v0 = d_srcb(r, c) - d_tarb(i, j);
				int v1 = d_srcg(r, c) - d_targ(i, j);
				int v2 = d_srcr(r, c) - d_tarr(i, j);
				double distanceRGB = (double)sqrtf(v0*v0 + v1*v1 + v2*v2) / (255.0*sqrtf(3));
				double distanceXY = sqrtf((r - i) * (r - i) + (c - j) * (c - j)) / sqrtf(numRows1*numRows1 + numCols1*numCols1);
				//double distance = lambda * distanceRGB + distanceXY;
				double distance =   distanceRGB ;

				//d_distance(r * numCols1 + c, i * numCols2 + j) = distance;
				d_distance(r * numCols1 + c, i * numCols2 + j) = distance;
			
		}
	}
}

__global__ void findMin(const PtrStepSz<double> d_distance, int size,  PtrStepSz<int> array1, int N, int numCols){
	
	int c = threadIdx.x;
	if (c < 0 || c >= N ) return;
	double minDistance = 0.1;
	for (int i = 0; i < size; i++){
		
			if (minDistance > d_distance(c, i)){
				minDistance = d_distance(c, i);
				array1(c, 0) = i;
			}
		
	}
}

__global__ void findMin2(const PtrStepSz<double> d_distance, int size, PtrStepSz<int> array1, int N, int numCols){
	int c = threadIdx.x;
	if (c < 0 || c >= N ) return;
	double minDistance = 0.1;
	for (int i = 0; i < size; i++){
		
			if (minDistance > d_distance(i, c)){
				minDistance = d_distance(i, c);
				array1(c, 0) = i;
			}
		
	}
}


__global__ void getCount(const PtrStepSz<int> array1, const PtrStepSz<int> array2, int N1, int N2, int* count){
	int c = threadIdx.x;
	if (c < 0 || c >= N2) return;
	if (array2(c, 0) >= 0 && array1(array2(c, 0), 0) == c)
				atomicAdd(count, 1);
}



double PatchMatching::match_gpu(){
	const dim3 blockSize(16, 16);
	dim3 gridSize((_src_patch_gpu.cols + blockSize.x - 1) / blockSize.x, (_src_patch_gpu.rows + blockSize.y - 1) / blockSize.y);

	GpuMat splitted_gpu_src[3];
	split(_src_patch_gpu, splitted_gpu_src);
	GpuMat splitted_gpu_tar[3];
	split(_tar_patch_gpu, splitted_gpu_tar);

	/*Mat temp1, temp2;
	_src_patch_gpu.download(temp1);
	_tar_patch_gpu.download(temp2);*/
	
	int numRows1 = _src_patch.rows;
	int numCols1 = _src_patch.cols;
	int numRows2 = _tar_patch.rows;
	int numCols2 = _tar_patch.cols;

	GpuMat d_distance(numRows1 * numCols1, numRows2 * numCols2, CV_64FC1);

	matching << <gridSize, blockSize >> >(splitted_gpu_src[0], splitted_gpu_src[1], splitted_gpu_src[2], splitted_gpu_tar[0], splitted_gpu_tar[1], splitted_gpu_tar[2], d_distance, numRows1, numCols1, numRows2, numCols2);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());

	/*Mat temp_distance;
	d_distance.download(temp_distance);*/


	int N1 = numRows1*numCols1;
	int N2 = numRows2*numCols2;
	GpuMat array1(N1, 1, CV_32SC1); array1.setTo(-1);
	GpuMat array2(N2, 1, CV_32SC1); array2.setTo(-1);
	findMin<<<1, N1>>>(d_distance, N2, array1, N1, numCols2);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());
	findMin2 << <1, N2 >> >(d_distance, N1, array2, N2, numCols1);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());

	/*Mat h_array1, h_array2;
	array1.download(h_array1);
	array2.download(h_array2);*/
	
	int h_count;
	int *d_count;
	cudaMalloc(&d_count, sizeof(int));
	getCount << <1, N2 >> >(array1, array2, N1, N2, d_count);
	cudaMemcpy(&h_count, d_count, sizeof(int), cudaMemcpyDeviceToHost);
	cudaFree(d_count);

	int smaller = N1 < N2 ? N1 : N2;
	return (double)h_count / (double)smaller;
}
